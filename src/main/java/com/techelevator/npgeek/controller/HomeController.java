package com.techelevator.npgeek.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.techelevator.npgeek.model.park.Park;
import com.techelevator.npgeek.model.park.ParkDao;
import com.techelevator.npgeek.model.survey.SurveyDao;
import com.techelevator.npgeek.model.survey.SurveyPost;
import com.techelevator.npgeek.model.survey.SurveyResults;
import com.techelevator.npgeek.model.weather.Weather;
import com.techelevator.npgeek.model.weather.WeatherDao;


@Controller
@SessionAttributes("degreeChoice")

public class HomeController {
	
	@Autowired
	ParkDao parkDao;
	
	@Autowired
	WeatherDao weatherDao;
	
	@Autowired
	SurveyDao surveyDao;

	
	@RequestMapping (path="/homePage", method=RequestMethod.GET)
	public String displayParkList(HttpServletRequest request) {
		List<Park> parkList = parkDao.getAllParks();
		
		request.setAttribute("parkList", parkList);
		return "homePage";	
	}

	
	@RequestMapping(path="/parkDetails", method=RequestMethod.GET) 
	public String displayParkByParkCode(HttpServletRequest request, ModelMap model){
		String parkCode = request.getParameter("parkCode");
		Park park = parkDao.getParkByParkCode(parkCode);
		
		model.addAttribute("parkCode", parkCode);
		request.setAttribute("park", park);
			
		return "parkDetails";
				
	}
		
	@RequestMapping (path="/weatherPage", method=RequestMethod.GET)
	public String displayWeatherList(HttpServletRequest request){

		List<Weather> weatherList = new ArrayList<Weather>();
		String parkCode = request.getParameter("parkCode");
		weatherList = weatherDao.getWeatherByParkCode(parkCode);
		
		request.setAttribute("weatherList", weatherList);
		return "weatherPage";
		
	}
	
	@RequestMapping(path="/weatherPage", method=RequestMethod.POST)
	public String processDegreeChoice(@RequestParam String degrees, 
										@RequestParam String code, 
										ModelMap modelMap){
		modelMap.addAttribute("degreeChoice", degrees);
		return "redirect:/weatherPage?parkCode=" + code;
		
	}
	
	@RequestMapping (path="/surveyInput", method=RequestMethod.GET)
	public String handleSurveyInput(HttpServletRequest request) {
		List<SurveyPost> surveyList = surveyDao.getAllPosts();
		
		request.setAttribute("surveyList", surveyList);
		return "surveyInput";
	}
	
	@RequestMapping(path="/surveyInput", method=RequestMethod.POST)
	public String processSurveySubmission(@RequestParam String emailAddress, 
											@RequestParam String state,
											@RequestParam String activityLevel,
											@RequestParam String favoritePark) {
		
		SurveyPost surveyPost = new SurveyPost();
		surveyPost.setEmail(emailAddress);
		surveyPost.setState(state);
		surveyPost.setActivityLevel(activityLevel);
		surveyPost.setFavoritePark(favoritePark);
		
		
		
		surveyDao.save(surveyPost);
		return "redirect:/surveyResult";
		
	
	}
	@RequestMapping("/surveyResult")
	public String handleSpaceForumResult(HttpServletRequest request) {
		
		List<SurveyResults> surveyList =  new ArrayList<SurveyResults>();		
		surveyList = surveyDao.getAllResults();
		request.setAttribute("surveyList", surveyList);
		
		
		return "surveyResult";
		
	}
		
	

}
