package com.techelevator.npgeek.model.survey;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;







@Component
public class JdbcSurveyDao implements SurveyDao{
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JdbcSurveyDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	@Override
	public List<SurveyPost> getAllPosts() {
		List<SurveyPost> allPosts = new ArrayList<>();
		String sqlSelectAllPosts = "SELECT * FROM survey_result";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllPosts);
		while(results.next()) {
			SurveyPost post = new SurveyPost();
			post.setEmail(results.getString("emailAddress"));
			post.setActivityLevel(results.getString("activityLevel"));
			post.setFavoritePark(results.getString("parkCode"));
			post.setState(results.getString("state"));
			post.setSurveyId(results.getInt("surveyId"));;
			allPosts.add(post);
		}
		return allPosts;
	}
	

	@Override
	public void save(SurveyPost surveyPost) {
		int surveyId = getNextSurveyId();
		String sqlInsertPost = "INSERT INTO survey_result(surveyId, parkCode, emailAddress, state, activityLevel) VALUES (?,?,?,?,?)";
		jdbcTemplate.update(sqlInsertPost, surveyId,surveyPost.getFavoritePark(),surveyPost.getEmailAddress(), surveyPost.getState(), surveyPost.getActivityLevel());
		surveyPost.setSurveyId(surveyId);
	}
	
	private int getNextSurveyId() {
		String sqlSelectNextId = "SELECT NEXTVAL('seq_surveyId')";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectNextId);
		int surveyId = 0;
		if(results.next()) {
			surveyId = results.getInt(1);
		} else {
			throw new RuntimeException("Something strange happened, unable to select next forum post id from sequence");
		}
		return surveyId;
	}
	@Override
	public List<SurveyResults> getAllResults() {
		List<SurveyResults> allResults = new ArrayList<>();
		String sqlGetSurveyResults = "SELECT parkName, "
									+ "COUNT(surveyId) AS votes "
									+ "FROM survey_result "
									+ "INNER JOIN park "
									+ "ON park.parkCode=survey_result.parkCode "
									+ "GROUP BY parkName "
									+ "ORDER BY votes DESC";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetSurveyResults);
		while(results.next()) {
			SurveyResults surveyResults = new SurveyResults();
			surveyResults.setParkName(results.getString("parkName"));
			surveyResults.setVotes(results.getInt("votes"));
			allResults.add(surveyResults);
		}
		return allResults;
	}
	

}
