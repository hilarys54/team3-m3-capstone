package com.techelevator.npgeek.model.survey;

import java.util.ArrayList;
import java.util.List;





public interface SurveyDao {
	public List<SurveyResults> getAllResults();
	public List<SurveyPost> getAllPosts();
	
	public void save(SurveyPost surveyPost);

}
