package com.techelevator.npgeek.model.survey;

public class SurveyPost {
	
	private String emailAddress;
	private String state;
	private String activityLevel;
	private int surveyId;
	private String favoritePark;
	
	
	
	
	public String getFavoritePark() {
		return favoritePark;
	}
	public void setFavoritePark(String favoritePark) {
		this.favoritePark = favoritePark;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmail(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getActivityLevel() {
		return activityLevel;
	}
	public void setActivityLevel(String activityLevel) {
		this.activityLevel = activityLevel;
	}
	public int getSurveyId(){
		return surveyId;
	}
	public void setSurveyId(int surveyId) {
		this.surveyId = surveyId;
	}
}
