package com.techelevator.npgeek.model.survey;

public class SurveyResults {
	
	private String parkName;
	private int votes;
	public String getParkName() {
		return parkName;
	}
	public void setParkName(String parkName) {
		this.parkName = parkName;
	}
	public int getVotes() {
		return votes;
	}
	public void setVotes(int votes) {
		this.votes = votes;
	}
	
}
