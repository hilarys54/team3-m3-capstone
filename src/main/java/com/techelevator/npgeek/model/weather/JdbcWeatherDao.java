package com.techelevator.npgeek.model.weather;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.npgeek.model.park.Park;

@Component
public class JdbcWeatherDao implements WeatherDao {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JdbcWeatherDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	
	
//  I DON'T THINK WE NEED THE FOLLOWING METHOD	
//	@Override
//	public List<Weather> getAllWeather() {
//		List<Weather> allWeather = new ArrayList<>();
//		String sqlSelectAllWeather= "SELECT * FROM weather";
//		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllWeather);
//		while(results.next()){
//			allParks.add(mapRowToPark(results));
//		}
//		return allParks;
//				
//	}

	@Override
	public List<Weather> getWeatherByParkCode(String parkCode) {
		List<Weather> allWeather = new ArrayList<>();
		String sqlSelectWeatherByParkCode = "SELECT * FROM weather WHERE parkCode=?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectWeatherByParkCode, parkCode);
		while(results.next()) {
			allWeather.add(mapRowToWeather(results));
		}
		
		return allWeather;
	}

	private Weather mapRowToWeather(SqlRowSet row) {
		Weather weather = new Weather();
		weather.setParkCode(row.getString("parkCode"));
		weather.setFiveDayForecast(row.getInt("fiveDayForecastValue"));
		weather.setLow(row.getInt("low"));
		weather.setHigh(row.getInt("high"));
		weather.setForecast(row.getString("forecast"));
		
		return weather;
	}



	

}
