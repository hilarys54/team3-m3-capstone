package com.techelevator.npgeek.model.weather;

public class Weather {
	
	private String parkCode;
	private int fiveDayForecast;
	private int low;
	private int high;
	private String forecast;
	private String forecastMessage;
	private String highLowMessage;
	
	public String getHighLowMessage() {
		if(high > 75) {
			highLowMessage = "Bring an extra galon of water.";
		} else if(high - low > 20) {
			highLowMessage = "Wear breathable layers.";
		} else if(low < 20) {
			highLowMessage = "WARNING: Frigid temperatures are coming and can be very dangerous!";
		}
		return highLowMessage;
	}
	
	public void setHighLowMessage(String highLowMessage) {
		this.highLowMessage = highLowMessage;
	}
	public String getForecastMessage() {
		if(forecast.equals("snow")) {
			forecastMessage = "Pack your snowshoes!";
		} else if(forecast.equals("rain")) {
			forecastMessage = "Pack rain gear and wear waterproof shoes!";
		} else if(forecast.equals("thunderstorms")) {
			forecastMessage = "Seek shelter and avoid hiking in exposed ridges.";
		} else if(forecast.equals("sunny")) {
			forecastMessage = "Pack sunblock!";
		}
		return forecastMessage;
	}
	public void setForecastMessage(String forecastMessage) {
		this.forecastMessage = forecastMessage;
	}
	public String getParkCode() {
		return parkCode;
	}
	public void setParkCode(String parkCode) {
		this.parkCode = parkCode;
	}
	public int getFiveDayForecast() {
		return fiveDayForecast;
	}
	public void setFiveDayForecast(int fiveDayForeCast) {
		this.fiveDayForecast = fiveDayForeCast;
	}
	public int getLow() {
		return low;
	}
	public void setLow(int low) {
		this.low = low;
	}
	public int getHigh() {
		return high;
	}
	public void setHigh(int high) {
		this.high = high;
	}
	public String getForecast() {
		return forecast;
	}
	public void setForecast(String forecast) {
		this.forecast = forecast;
	}

}
