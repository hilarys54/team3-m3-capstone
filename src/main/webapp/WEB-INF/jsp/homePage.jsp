<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@include file="/WEB-INF/jsp/common/header.jsp"%>

<title>HomePage</title>



<ul class="cbp-ig-grid">
<c:forEach var="park" items="${parkList}">
	<li style="background-image:url('img/parks/${park.parkCode.toLowerCase()}.jpg');  -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;">
		<a href="parkDetails?parkCode=${park.parkCode}">
				<span class="cbp-ig-icon cbp-ig-icon-img"><br></span>
			<h3 class="cbp-ig-title">${park.parkName}</h3>
			<span class="cbp-ig-category">${park.state}<br>
			${park.parkDescription}
			  </span>

			
			
		</a>
	</li>
	</c:forEach>
</ul>








	<%-- <div id="grid-gallery" class="grid-gallery">
	<section class="grid-wrap">
	<ul class="grid">
	<c:forEach var="park" items="${parkList}">
		<li class="grid-sizer"></li><!-- for Masonry column width -->
			<li>
				<figure>
					<img src="img/parks/${park.parkCode.toLowerCase()}.jpg">
					<figcaption><h2>${park.parkName}</h2><p>${park.parkDescription}</p></figcaption>
				</figure>
			</li>
			</c:forEach>
	</ul>
	</section>
		<section class="slideshow">
		<ul>
			<c:forEach var="park" items="${parkList}">
			<li>
				<figure>
					<figcaption>
					<h2><a href="parkDetails?parkCode=${park.parkCode}">${park.parkName}</a></h2>
	
	<h3> ${park.state} </h3>
	<p>${park.parkDescription} </p>
					</figcaption>
					<img class="parkListImg" src="img/parks/${park.parkCode.toLowerCase()}.jpg">
				</figure>
			</li>
	 --%>
  <%--  
        <a class="rig-cell" href="#"  onclick="show('popup1')">
           <img class="rig-img" src="img/parks/${park.parkCode.toLowerCase()}.jpg">
            <span class="rig-overlay"></span>
            <span class="rig-text">${park.parkName}</span>
           
        </a>
        
        <div class="popup" id="popup1">
<h2><a href="parkDetails?parkCode=${park.parkCode}">${park.parkName}</a></h2> 
 	<img class="parkListImg" src="img/parks/${park.parkCode.toLowerCase()}.jpg"> 
 	<h3> ${park.state} </h3>
	<p>${park.parkDescription} </p>
  <a href="#" onclick="hide('popup1')">Ok!</a>
</div> --%>

<%-- 
	<a href="" data-largesrc="img/parks${park.parkCode.toLowerCase()}.jpg" data-title="${park.parkName}" data-description="${park.parkDescription}">
		<img style="height:200px;"  src="img/parks/${park.parkCode.toLowerCase()}.jpg">
	</a>
	
	<div class="og-expander">
		<div class="og-expander-inner">
			<span class="og-close"></span>
			<div class="og-fullimg">
				<div class="og-loading"></div>
				<img src="img/parks/${park.parkCode.toLowerCase()}.jpg">
			</div>
			<div class="og-details">
				<h3><a href="parkDetails?parkCode=${park.parkCode}">${park.parkName}</a></h3>
				<p>${park.parkDescription} </p>
				<a href="http://cargocollective.com/jaimemartinez/">Visit website</a>
			</div>
		</div>
	</div>
	<h2><a href="parkDetails?parkCode=${park.parkCode}">${park.parkName}</a></h2>
	<img class="parkListImg" src="img/parks/${park.parkCode.toLowerCase()}.jpg">
	<h3> ${park.state} </h3>
	<p>${park.parkDescription} </p>
	</li> --%>
<%-- 	</c:forEach>
	</ul>
		<nav>
			<span class="icon nav-prev"></span>
			<span class="icon nav-next"></span>
			<span class="icon nav-close"></span>
		</nav>
		<div class="info-keys icon">Navigate with arrow keys</div>
	</section>
</div> --%>
	
	<%@include file="/WEB-INF/jsp/common/footer.jsp"%>
	
	