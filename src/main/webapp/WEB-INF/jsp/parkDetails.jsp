<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@include file="/WEB-INF/jsp/common/header.jsp"%>

<title>Park Detail</title>

<div id = "parkDetails">

<h1>${park.parkName}</h1>

<img class="parkListImg" src="img/parks/${park.parkCode.toLowerCase()}.jpg">
<h3>${park.state}</h3>
<a href="weatherPage?parkCode=${park.parkCode}">${park.parkName} Click here to show this park's weather in:</a>
<br>

<ul>

<li><b>Description:</b> ${park.parkDescription}</li>
<li><b>Acreage:</b> ${park.acreage}</li>
<li><b>Elevation:</b> ${park.elevationInFeet}</li>
<li><b>Miles of Trail:</b> ${park.milesOfTrail}</li>
<li><b>Number of Campsites:</b> ${park.numberOfCampsites}</li>
<li><b>Climate:</b> ${park.climate}</li>
<li><b>Year Founded:</b>${park.yearFounded}</li>
<li><b>Annual Visitor Count: </b>${park.annualVisitorCount}</li>
<li><b>${park.inspirationalQuote}</b> Source: ${park.inspirationalQuoteSource}</li>
<li><b>Entry Fee:</b> ${park.entryFee}</li>
<li><b>Number of Animal Species:</b> ${park.numberOfAnimalSpecies}</li>
</ul>






</div>




<%@include file="/WEB-INF/jsp/common/footer.jsp"%>