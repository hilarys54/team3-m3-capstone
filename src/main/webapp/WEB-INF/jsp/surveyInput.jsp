<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@include file="/WEB-INF/jsp/common/header.jsp"%>

<title>Survey Input</title>
<h1 id="title">Survey</h1>
<div id="survey">

<c:url var="surveyPost" value="/surveyInput" /></c>
			<form action="${surveyPost}" method="POST">

<label>Email Address</label> <input type="text" name="emailAddress" id="email" /><br>
<label>State</label>
<input type="text" name="state" id="state" /><br>
<label>Park</label>
<select name="favoritePark" id="favoritePark">
	
		<option  value= "CVNP">Cuyahoga Valley National Park</option>
		<option  value= "ENP">Everglades National Park</option>
		<option value= "GCNP">Grand Canyon National Park</option>
		<option  value= "GNP">Glacier National Park</option>
		<option  value= "GSMNP">Great Smokey Mountains National Park</option>
		<option  value= "GSMNP">Grand Teton National Park</option>
		<option  value= "MRNP">Mount Rainier National Park</option>
		<option  value= "RMNP">Rocky Mountain National Park</option>
		<option  value= "YNP">Yellowstone National Park</option>
		<option value= "YNP2">Yosemite National Park</option>
		
		
		
		
</select><br>
<label>Activity Level</label>
<select name="activityLevel" id="activityLevel">
	
		<option value="inactive">Inactive</option>
		<option value="sedentary">Sedentary</option>
		<option value="active">Active</option>
		<option value="extremely active">Extremely Active</option>
		</select><br>
		<input type="submit" value="Submit!" />
		
	

</form>
</div>








<%@include file="/WEB-INF/jsp/common/footer.jsp"%>