<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@include file="/WEB-INF/jsp/common/header.jsp"%>

<title>Weather Page</title>


<div id = "weatherPage">

<h1>${park.parkName}</h1>
<form action="${degrees}" method="POST">
<input type="radio" name="degrees" value="fahrenheit">Fahrenheit<br>
<input type="radio" name="degrees" value="celsius">Celsius<br>
<input type="hidden" name="code" value="${param.parkCode}">
<input type="submit" value="Show my park's weather!">
</form>

<c:if test="${empty degreeChoice }">
<c:set var="degreeChoice" value="fahrenheit"/>
</c:if>

<table>
<tr>
<th>Date</th>
<th>Image</th>
<th>Low</th>
<th>High</th>
<th>Forecast Message</th>
</tr>
<c:forEach var="day" items="${weatherList}">
<tr>
<td>${day.fiveDayForecast} </td>
<td><img></td>

<td>${day.low}</td>
<td>${day.high}</td>
<td>${day.forecast}<br>${day.highLowMessage} <br>${day.forecastMessage}</td>
<td><img class="weatherImg" src="img/weather/${day.forecast}.png"></td>

</tr>
</c:forEach>
</table>

 



</div>




<%@include file="/WEB-INF/jsp/common/footer.jsp"%>